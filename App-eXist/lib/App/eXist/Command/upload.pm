package App::eXist::Command::upload;

use v5.30;

use App::eXist -command;
use DateTime;
use strict;
use warnings;
use MIME::Types;
use Term::ANSIColor;
use Path::Tiny;

sub abstract { "uploads a file" }

sub description { "Uploads local file to remote server."}

sub opt_spec {
    return ();
}

sub validate_args {
    my ($self, $opt, $args) = @_;

    $self->usage_error("You must supply a target path and a source file.") if scalar(@$args) != 2;
    $self->{server} = $self->app->connect;
}

sub execute {
    my ($self, $opt, $args) = @_;

    my ($local, $remote) = @$args;

    die colored("Can't find local file '$local'", 'bright_red') unless -f $local;

    my $mt = MIME::Types->new();
    my $mimetype = $mt->mimeTypeOf($local);

    my ($rc, $ok) = $self->{server}->uploadDocument( $remote, $local,  mime_type => $mimetype);

    die $ok if $rc;
    say colored("Successfully stored file as $remote", 'bright_green');
}

1;
