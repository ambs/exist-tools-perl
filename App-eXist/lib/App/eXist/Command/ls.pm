package App::eXist::Command::ls;

use v5.30;

use App::eXist -command;
use DateTime;

use strict;
use warnings;
use Path::Tiny;
use Term::ANSIColor;

sub abstract { "lists a remote folder" }

sub description { "Given a folder name, lists its contents."}

sub opt_spec {
    return ();
}

sub validate_args {
    my ($self, $opt, $args) = @_;

    $self->usage_error("You must supply exactly one path.") if scalar(@$args) != 1;
    $self->{server} = $self->app->connect;
}

sub execute {
    my ($self, $opt, $args) = @_;

    my $prefix = $args->[0];
    say colored("Listing $prefix", 'bright_blue');

    my ($rc, $descr) = $self->{server}->describeCollection($prefix, documents => 1);
    die "$descr\n" if $rc;

    if (exists($descr->{collections})) {
        for my $col (sort { $a cmp $b } @{$descr->{collections}}) {

            my ($rc1, $descr1) = $self->{server}->describeCollection( path($prefix, $col), documents => 0);
            die "$descr1\n" if $rc1;

            printf(" C  %-30s %-10s %-10s\n", $col, map { $descr1->{$_} } qw.owner group.);
        }
    }

    if (exists($descr->{documents})) {
        my $documents = $descr->{documents};
        for my $k (sort { $a cmp $b } keys %$documents) {
            my $doc = $documents->{$k};
            printf(" -  %-30s %-10s %-10s\n", map { $doc->{$_} } qw.name owner group.);
        }
    }

}

1;
