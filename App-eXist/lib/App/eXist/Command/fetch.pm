package App::eXist::Command::fetch;

use v5.30;

use App::eXist -command;
use DateTime;
use Path::Tiny;
use strict;
use warnings;
use Term::ANSIColor;

sub abstract { "retrieves a remote file" }

sub description { "Given a file name, retrieve its contents."}

sub opt_spec {
    return (
        ['force|f' => "Force file overwrite"],
       );
}

sub validate_args {
    my ($self, $opt, $args) = @_;

    $self->usage_error("You must supply exactly one path.") if scalar(@$args) != 1;
    $self->{server} = $self->app->connect;
}

sub execute {
    my ($self, $opt, $args) = @_;
    my $filename = path($args->[0])->basename;

    $filename = path($filename);
    if (-f $filename && !$opt->{force}) {
        say colored("Won't overwrite $filename without force.", 'bright_red');
    }
    else {
        my ($rc, $bytes) = $self->{server}->downloadDocument($args->[0]);
        die "$bytes\n" if $rc;

        path($filename)->spew_raw($bytes);
        say colored("Stored successfully on $filename.", 'bright_green');
    }
}

1;
