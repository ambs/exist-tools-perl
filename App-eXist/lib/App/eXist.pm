# ABSTRACT: main module for x-db command line tool.

use strict;
use warnings;
package App::eXist;


use v5.30;
use experimental 'signatures';
use App::Cmd::Setup -app;

use Path::Tiny;
use YAML::XS;
use XML::eXistDB::RPC;


sub connect($app) {
    my $config = $app->config;

    die "No default configuration found\n" unless exists($config->{default}) and exists($config->{servers}{$config->{default}});

    my $c = $config->{servers}{$config->{default}};
    $app->{server} = XML::eXistDB::RPC->new( user => $c->{username}, password => $c->{password}, destination => sprintf("%s://%s:%d/exist/xmlrpc", map {$c->{$_}} qw.scheme hostname port.));
}

sub config($app) {

    if (exists($app->global_options->{config})) {
        return _load_config($app->global_options->{config});
    } else {
        return _config();
    }

}

sub _load_config($filename) {
    die "Could not read file: $filename" unless -f $filename;
    return Load(path($filename)->slurp);
}

sub _config {
    my $home = path($ENV{HOME});

    my $config_file = undef;
    if (-d $home) {
        for (qw!.x-db.yml .x-db.yaml!) {
            $config_file = path($home, $_) if -f path($home, $_);
            last;
        }
    }    
    
    if ($config_file) {
        return _load_config($config_file);
    }

    return {};
}

sub global_opt_spec {
    return (['config|c=s' => "Specify configuration file"]);
}




1;
